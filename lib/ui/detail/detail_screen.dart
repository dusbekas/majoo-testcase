import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:majootestcase/models/movie_model.dart';

class DetailScreen extends StatelessWidget {
  const DetailScreen({Key key, this.data}) : super(key: key);

  final Results data;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.network("https://image.tmdb.org/t/p/w500/" + data.posterPath),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                      data.name ??
                          data.title +
                              ' (' +
                              (data.firstAirDate != null
                                  ? data.firstAirDate.substring(0, 4)
                                  : data.releaseDate.substring(0, 4) + ')'),
                      textDirection: TextDirection.ltr,
                      style: Theme.of(context).textTheme.headline5),
                  SizedBox(height: 16,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Row(
                        children: [
                          Icon(Icons.star),
                          SizedBox(width: 4),
                          Text(data.voteAverage.toString() + '/10'),
                        ],
                      ),
                      SizedBox(width: 16),
                      Row(
                        children: [
                          Icon(Icons.trending_up),
                          SizedBox(width: 4),
                          Text(data.popularity.toString()),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 16,),
                  Text(data.overview, textAlign: TextAlign.justify,),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
