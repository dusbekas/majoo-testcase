import 'package:flutter/material.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/models/movie_model.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
   final List<Results> data;

  const HomeBlocLoadedScreen({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, index) {
          return index == data.length -1
              ? Column(
                children: [
                  movieItemWidget(data[index], context),
                  SizedBox(height: 16),
                  CustomButton(
                    text: 'Logout',
                    onPressed: () => handleLogout(context),
                    height: 100,
                    isSecondary: true,
                  )
                ],
              )
              : movieItemWidget(data[index], context);
        },
      ),
    );
  }

  Widget movieItemWidget(Results data, BuildContext context){
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)
          )
      ),
      child: InkWell(
        onTap: () => HomeBlocCubit().moveToDetail(context, data),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(25),
              child: Image.network("https://image.tmdb.org/t/p/w500/"+data.posterPath),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
              child: Text(data.name ?? data.title, textDirection: TextDirection.ltr),
            )
          ],
        ),
      ),
    );
  }

  void handleLogout(BuildContext context) async {
    AuthBlocCubit authBlocCubit = AuthBlocCubit();
    authBlocCubit.logOutUser(context);
  }
}
