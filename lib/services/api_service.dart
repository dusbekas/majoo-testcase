import 'dart:io';

import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

class ApiServices{

  Future<MovieModel> getMovieList() async {
    try {
      var dio = await dioConfig.dio();
      Response response = await dio.get(dio.options.baseUrl);

      // return MovieModel();
      print('ok1');
      if (response.statusCode == HttpStatus.ok) {
        return MovieModel.fromJson(response.data);
        // TODO: Handle error
      } else {
        return null;
      }
    } catch(e) {
      print('ok2');
      print(e.toString());
      // if (e == DioErrorType.DEFAULT)
        return MovieModel(errorType: e);
        //return null;
    }
  }
}