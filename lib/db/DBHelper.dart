import 'dart:io';

import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/models/user_constant.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBHelper {
  static Database database;

  Future<Database> get db async =>
      database ??= await initDb();

  Future<Database> initDb() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path, dbName);
    var db = await openDatabase(path, version: 1, onCreate: _onCreate);
    return db;
  }

  _onCreate(Database db, int version) async {
    await db.execute(
        'CREATE TABLE $tableUsers($columnEmail TEXT PRIMARY KEY, $columnUsername TEXT UNIQUE, $columnPassword TEXT)');
    await db.insert(
      tableUsers,
      User(email: 'majoo@gmail.com', password: '123456', userName: 'majoo')
          .toJson(),
      // conflictAlgorithm: ConflictAlgorithm.fail,
    );
  }

  Future<void> insertUser(User user) async {
    // Get a reference to the database.
    final dbClient = await db;

    await dbClient.insert(
      'user',
      user.toJson(),
      // conflictAlgorithm: ConflictAlgorithm.fail,
    );
  }

  /*Future<List<User>> getUsers() async {
    var dbClient = await db;
    List<Map<String, dynamic>> maps =
        await dbClient.query(tableUsers, columns: [
      columnEmail,
      columnUsername,
      columnPassword,
    ]);

    List<User> users = [];

    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        users.add(User.fromJson(maps[i]));
      }
    }
    return users;
  }

  Future<dynamic> getUser(String nationalId) async {
    final dbClient = await db;
    return await dbClient.query(USER_TABLE,
        where: "$NATIONAL_ID = ?", whereArgs: [nationalId], limit: 1);
  }*/

  Future<User> checkLogin(String email, String password) async {
    final dbClient = await db;
    /*var res = await dbClient.rawQuery(
        "SELECT * FROM $tableUsers WHERE $columnEmail = '$email' AND $columnPassword = '$password'");*/
    var res = await dbClient.rawQuery(
        "SELECT * FROM $tableUsers WHERE $columnEmail = '$email' AND $columnPassword = '$password'");

    if (res.length > 0) {
      return new User.fromJson(res.first);
    }

    return null;
  }



  Future<User> registerUser(User user) async {
    final dbClient = await db;
    String email = user.email;

    /// Check the email first
    var res = await dbClient.rawQuery(
        "SELECT * FROM $tableUsers WHERE $columnEmail = '$email'");
    print(res);
    if (res.length > 0) {
      return null;
    } else {
      await insertUser(user);

      return await checkLogin(user.email, user.password);
    }
  }

  /*Future<int> delete(String nationalId) async {
    var dbClient = await db;
    return await dbClient
        .delete(USER_TABLE, where: '$nationalId = ?', whereArgs: [nationalId]);
  }

  Future<int> update(User user) async {
    var dbClient = await db;
    return await dbClient.update(USER_TABLE, user.toMap(),
        where: '$NATIONAL_ID = ?', whereArgs: [user.nationalId]);
  }*/

  Future close() async {
    var dbClient = await db;
    dbClient.close();
  }
}