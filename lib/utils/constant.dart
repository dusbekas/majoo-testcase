class Preference {
  static const USER_INFO = "user-info";
}

class Api {
  static const BASE_URL = "";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
}

class Font {
}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}


const EMAIL_ERROR_FORMAT = 'Masukkan e-mail yang valid';
const FORM_ERROR = 'Form tidak boleh kosong, mohon cek kembali data yang Anda'
    ' inputkan';