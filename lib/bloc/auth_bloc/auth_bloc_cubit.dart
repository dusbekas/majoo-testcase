import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/db/DBHelper.dart';
import 'package:majootestcase/main.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchLoginHistory() async{
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if(isLoggedIn== null){
      emit(AuthBlocLoginState());
    }else{
      if(isLoggedIn){
        emit(AuthBlocLoggedInState());
      }else{
        emit(AuthBlocLoginState());
      }
    }
  }

  void loginUser(User user, BuildContext context) async{
    emit(AuthBlocLoadingState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    User authToDb = await DBHelper().checkLogin(user.email, user.password);
    if (authToDb != null) {
      await sharedPreferences.setBool("is_logged_in",true);
      String data = user.toJson().toString();
      sharedPreferences.setString("user_value", data);
      // emit(AuthBlocLoggedInState());

      Fluttertoast.showToast(
        msg: "Login Berhasil",
        toastLength: Toast.LENGTH_LONG,
      );
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (_) => BlocProvider(
            create: (context) => HomeBlocCubit()..fetchMovies(),
            child: HomeBlocScreen(),
          ),
        ),
      );
    } else {
      Fluttertoast.showToast(
        msg: "Login gagal, periksa kembali inputan Anda",
        toastLength: Toast.LENGTH_LONG,
      );
    }
  }

  void logOutUser(BuildContext context) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setBool("is_logged_in", null);
    await sharedPreferences.setString("user_value", null);

    Fluttertoast.showToast(
      msg: "Logout Berhasil",
      toastLength: Toast.LENGTH_LONG,
    );
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(
        builder: (_) => BlocProvider(
          create: (context) =>
          AuthBlocCubit()..fetchLoginHistory(),
          child: MyHomePageScreen(),
        )
      ),
      (Route<dynamic> route) => false,
    );
  }

  void registerUser(User user, BuildContext context) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    User authToDb = await DBHelper().registerUser(user);
    if (authToDb != null) {
      await sharedPreferences.setBool("is_logged_in", true);
      String data = user.toJson().toString();
      sharedPreferences.setString("user_value", data);

      Fluttertoast.showToast(
          msg: "Register Berhasil",
          toastLength: Toast.LENGTH_LONG,
      );
      Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
          builder: (_) => BlocProvider(
            create: (context) => HomeBlocCubit()..fetchMovies(),
            child: HomeBlocScreen(),
          ),
        ),
        (Route<dynamic> route) => false,
      );
    }
  }
}
