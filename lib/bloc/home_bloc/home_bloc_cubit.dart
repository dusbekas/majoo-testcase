import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/services/api_service.dart';
import 'package:majootestcase/ui/detail/detail_screen.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());

  void fetchMovies() async {
    emit(HomeBlocInitialState());
    ApiServices apiServices = ApiServices();
    MovieModel movieResponse = await apiServices.getMovieList();

    if(movieResponse==null){
      emit(HomeBlocErrorState(errorMessage: "Error Unknown"));
    } else if (movieResponse.errorType?.type == DioErrorType.DEFAULT){
      emit(HomeBlocErrorState(
        errorMessage: "Koneksi internet error. Perika koneksi internet",
        iconData: Icons.cloud_off,
        refreshFunction: fetchMovies,
      ));
    } else{
      emit(HomeBlocLoadedState(movieResponse.results));
    }
  }

  void moveToDetail(BuildContext context, Results data) async {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => DetailScreen(data: data),
      ),
    );
  }
}
